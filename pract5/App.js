import React, { Component } from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";
import List from './List.js'

class App extends Component {
  state = {
    data: [{
      key: 0
    }],
    timer: ''
  };
  deleteItem = (i) => {
    let data = this.state.data
    data.splice(i,1)
    clearTimeout(this.state.timer)
    
    this.setState({data, timer: setTimeout(() => {
      alert('Alert!')
    }, 10000)})
  }
  componentDidMount = () => {
    fetch("https://uinames.com/api/?region=russia&ext&amount=8", {
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        return responseJson.map(item => {
          item.birthday = item.birthday.dmy
          return item
        })
      })
      .then(responseJson => {
        console.log(responseJson);
        this.setState({
          data: responseJson
        });
        this.setState({timer: setTimeout(() => {
          alert('Alert!')
        }, 10000)})
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <List elements={this.state.data} alertItemName={this.deleteItem}></List>
      </View>

    );
  }
}
export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
