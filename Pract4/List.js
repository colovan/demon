import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, TextInput } from 'react-native'
class List extends Component {
    state = {
        elements: [
            {
              id: 0,
              content: 'Llalalala',
            },
            {
              id: 1,
              content: 'Lolollololo',
            },
            {
              id: 2,
              content: 'Hellooooo',
            },
            {
              id: 3,
              content: 'I am machine',
            }
          ],
        chosed: [],
        colorChosed: {},
        content: ''
    }
    toggleListItem = id => {
        let chosed = this.state.chosed
        let colorChosed = this.state.colorChosed

        if (chosed.indexOf(id) != -1) {
            chosed.splice(chosed.indexOf(id),1)
            delete colorChosed[id]
        }  else {
            chosed.push(id)
            colorChosed[id] = '#'+Math.floor(Math.random()*16777215).toString(16)
        } 
        this.setState({chosed: chosed, colorChosed: colorChosed})
    }
    handleContent = text => {
        this.setState({content: text})
    }
    changeContentChosed = () => {
        let elements = this.state.elements
        elements = elements.map((item) => {
            if (this.state.chosed.indexOf(item.id) != -1) {
                return {id: item.id, content: this.state.content, color: item.color}
            }  
            else {
                return {id: item.id, content: item.content}
            }
        })
        this.setState({elements: elements})
    }

 render() { 
    return (
        <View style={{width: '80%', marginLeft: '10%'}}>
            {
            this.state.elements.map((item) => (
            <TouchableOpacity
            key = {item.id}
            style = {[{backgroundColor: this.state.chosed.indexOf(item.id) != -1? this.state.colorChosed[item.id]: '#d9f9b1'}, styles.container]}
            onPress = {
                () => this.toggleListItem(item.id)
                }
            >
            
            <Text style = {styles.text}>
            {item.content}
            </Text>
            </TouchableOpacity>
            ))
            }
            { this.state.chosed.length > 0 &&
                <View style={{marginTop: '10%'}}>
                    <TextInput style={{borderColor: '#a4ff33', borderWidth: 2, textAlign: 'center'}}
                    placeholder="Введите что-нибудь"
                    onChangeText = {this.handleContent}
                    value = {this.state.content}>
                    </TextInput>
                    <TouchableOpacity style={{borderColor: '#ddd', borderWidth: 5, marginTop: '10%', height: '35%'}}
                    onPress = {
                        () => this.changeContentChosed()
                    }>
                        <Text style={{color: '#a4ff33', textAlign: 'center', fontSize: 30, fontWeight: 'bold'}}>Изменить</Text>
                    </TouchableOpacity>
                </View>
            }
        </View>
    )
 }
}
export default List
const styles = StyleSheet.create ({
 container: {
    padding: 10,
    marginTop: 3,
    alignItems: 'center',
 },
 text: {
    color: '#4f603c'
 },
 handler: {
     marginTop: '10%',
      
 }
})